var restify = require('restify'); 

var http = require("http"),
    url = require("url"),
    path = require("path"), 
    fs = require('fs'),
    bodyParser = require('body-parser')

var port = 8080; 

// модуль для обработки запросов 
var apiHandler = require('./api_handler'); 

// создание сервера 
var server = restify.createServer({
    name: 'myApp'
});
server.pre(function(req, res, next) {
    req.headers.accept = 'application/json';
    return next();
  });
// middleware для обработки тела запроса 
server.use(restify.plugins.bodyParser({ mapParams: true }));
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());

server.use(function(req, res, next) {

    console.log(req.method + ' ' + req.url); 
    next(); 
})

server.get('/', function (req, res, next) {
    var filePath = path.join(__dirname, 'index.html');
    fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
      if (err) {
            res.writeHead(500, {'Content-Type': 'text/html'});
            res.end('Error loading index.html');
        } else {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(data);
        }
    });
    //next();
}); 


// выбрать все элементы 
server.get('/api', apiHandler.loadItems);

// создать новый элемент 
server.post('/api/new', apiHandler.createItem);

// просмотреть элемент с указанным ID
server.get('/api/:id', apiHandler.getItemById);
    

// обновить элемент по ID 
server.put('/api/:id', apiHandler.updateItem);
    

// удалить элемент по ID 
server.del('/api/:id', apiHandler.removeItem);
    
// обработчик ошибок 
server.on('InternalServer', function(err) {
    err.body = 'oops...error'; 
    res.send(err); 
});

server.listen(port, function () {
    console.log('server running on port ' + port); 
})