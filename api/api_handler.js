// соединение с бд 
//var pool = require('./db_handler');
var mysql = require('mysql2');
var path = require('path'); 
var config = {
    user : 'test',
    password : '1234512345',
    database : 'testdb',
    host : 'localhost',
    port : 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
};

// Створюємо пул з'єднань
var pool = mysql.createPool(config);
module.exports = {
    // загрузка всех элементов
    loadItems: function (req, res) {

		// подключение к бд 
		pool.getConnection(function(err, connection) {
			if (err) {
				console.log(err);
				return;
			}

			connection.query("SELECT * FROM items", function(err, rows) {
				connection.release(); // Звільняємо з'єднання назад у пул
				if (err) {
					console.log(err);
					return;
				}
				console.log('GET ' + req.url);
				res.status(200);
				res.json(rows); 
			}); 
		});

    },
    // загрузка элемента из бд по id 
    getItemById: function (req, res) {

		// подключение к бд 
		pool.getConnection(function(err, connection) {
			if (err) {
				console.log(err);
				return;
			}
	
			var inserts = [parseInt(req.params.id)];
			var sql = "SELECT * FROM items WHERE id = ?";
			connection.query(sql, inserts, function(err, rows) {
				connection.release();
				if (err) {
					console.log(err);
					return;
				}
				console.log('GET ' + req.url);
				res.status(200);
				res.json(rows);  
			});
		});
    },
    // создание элемента 
    createItem: function (req, res) {

        // подключение к бд  
		pool.getConnection(function(err, connection) {
			if (err) {
				console.log(err);
				return;
			}

			var data = req.body;
			var c = data.complete? 1:0;
	        var inserts = [data.name, data.description, c];
			//console.log(inserts);
			var sql = "INSERT INTO items (name, description, complete) VALUES (?, ?, ?)";
			// Виконуємо запит INSERT
			connection.query(sql, inserts, function(err, results) {
				connection.release(); // Звільняємо з'єднання назад у пул
				if (err) {
					console.log(err);
					return;
				}
				console.log('POST ' + req.url);
				res.status(200).send("alert('sample item added to database')");
			}); 
		});
    },
    // обновление элемента (редактирование) 
    updateItem: function (req, res) { 
	
		pool.getConnection(function(err, connection) {
			if (err) {
				console.log(err);
				return;
			}
			var data = req.body;
			var c = data.complete? 1:0;
	        var inserts = [data.name, data.description, c, data.id];

			// Виконуємо запит UPDATE
			connection.query("UPDATE items SET name=?, description=?, complete=? WHERE id=?", inserts, function(err, results) {
				connection.release();
				if (err) {
					console.log(err);
					return;
				}
				console.log('PUT ' + req.url);
				res.status(200).send("alert('item by id ' + req.params.id + ' changed')");
			});
		});
    },
    // удаление элемента 
    removeItem: function (req, res) {

		pool.getConnection(function(err, connection) {
			if (err) {
				console.log(err);
				return;
			}
	
			// Виконуємо запит DELETE
			connection.query("DELETE FROM items WHERE id=?", [parseInt(req.params.id)], function(err, results) {
				connection.release();
				if (err) {
					console.log(err);
					return;
				}
				console.log('DELETE ' + req.url);
				res.status(200).send('item deleted from database');
			});
		});
    }
}